package shapes3d;

import shapes2d.Circle;

public class Cylinder extends Circle {
    double h;
    public Cylinder(double r,double h){
        super(r);
        this.h = h;

    }

    @Override
    public double area() {
        return super.area()+(2*3.14*r*h);
    }
    public double volume(){
        return super.area()*h;
    }
    public void printArea(){
        System.out.println("Radius of the cylinder is "+r+" Height of the cylinder is "+h+" area of the cylinder is "+area()+" volume of the cylinder is "+volume());
    }

}