package shapes3d;

import shapes2d.Square;

public class Cube extends Square {

    public Cube(double side){
        super(side);
    }
    public double area(){
        return super.area()*6;
    }
    public double volume(){
        return  super.area()*side;
    }
    public void printArea(){
        System.out.println("One side of the cube is "+side+" area of the cube is " +area()+" volume of the circle is "+volume());
    }


}
