package shapes2d;

public class Circle {
    public double r;
    public Circle(double r){
        this.r = r;
    }

    public double area(){

        return 3.14 * r * r;
    }
    public void printArea(){
        System.out.println("Radius of the circle is "+r);
        System.out.println("Area of the circle is "+area());

    }

}
