package shapes2d;

public class Square {
    public double side;

    public Square(double side){
        this.side = side;
    }
    public double area(){
        return side*side;
    }
    public void printArea(){
        System.out.println("One side of the square is "+side);
        System.out.println("Area of the square is "+area());

    }

}