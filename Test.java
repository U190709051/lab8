package shapes2d;

import shapes3d.Cube;
import shapes3d.Cylinder;

public class Test {
    public static void main(String[] args) {
        Circle testCircle = new Circle(4);
        testCircle.printArea();

        Square testSquare = new Square(4);
        testSquare.printArea();

        Cylinder testCylinder = new Cylinder(4,4);
        testCylinder.printArea();

        Cube testCube = new Cube(4);
        testCube.printArea();
    }
}
